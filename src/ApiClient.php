<?php

namespace ProPhp\GitlabApi;

use ProPhp\Curl\Curl;
use ProPhp\Curl\CurlParams;
use ProPhp\Curl\JsonParams;

class ApiClient
{
    private string $privateToken;

    private string $host;

    private array $user = [];

    private array $projects = [];

    public function __construct(string $privateToken, string $host = 'gitlab.com')
    {
        $this->privateToken = $privateToken;
        $this->host = $host;
    }

    private function setUserData(): void
    {
        $response = Curl::json("https://{$this->host}/api/v4/user?private_token={$this->privateToken}");

        if (empty($response['id'])) {
            throw new \Exception("Failed to fetch user data");
        }

        $this->user = $response;
    }

    private function setProjectsData(bool $compactResponse = true): void
    {
        $response = $this->listProjects($compactResponse);

        // @todo Improve validation later
        if (empty($response)) {
            throw new \Exception("Failed to fetch projects data");
        }

        $this->projects = $response;
    }

    public function listProjects(bool $compactResponse = true): array
    {
        $result = [];

        if (empty($this->user)) {
            $this->setUserData();
        }

        $response = Curl::json(
            "https://{$this->host}/api/v4/users/{$this->user['id']}/projects?private_token={$this->privateToken}&per_page=100&page=1"
        );

        if(count($response) === 100){
            throw new \Exception("Project amount limit is reached");
        }

        if ($compactResponse) {
            foreach ($response as $projectData) {
                $result[$projectData['path']] = [
                    'id' => $projectData['id'],
                    'name' => $projectData['name'],
                    'name_with_namespace' => $projectData['name_with_namespace'],
                    'description' => $projectData['description'],
                    'path_with_namespace' => $projectData['path_with_namespace'],
                    'http_url_to_repo' => $projectData['http_url_to_repo']
                ];
            }
        }

        return $compactResponse ? $result : $response;
    }

    private function createLabel(string|int $projectPathOrId, array $labelConfig): array
    {
        $projectId = is_string($projectPathOrId) ? $this->getProjectId($projectPathOrId) : $projectPathOrId;

        $response = Curl::json(
            "https://{$this->host}/api/v4/projects/$projectId/labels",
            (new CurlParams())
                ->method('POST')
                ->headers(["PRIVATE-TOKEN: " . $this->privateToken])
                ->urlQueryData($labelConfig)
        );

        return $response;
    }

    public function createLabels(string|int $projectPathOrId, array $labelsConfig): array
    {
        $response = [];

        foreach ($labelsConfig as $labelConfig) {
            $singleActionResponse = $this->createLabel($projectPathOrId, $labelConfig);
            $response[] = [
                'status' => @$singleActionResponse['message'] === 'Label already exists' ? 'failure' : 'success',
                'request' => $labelConfig,
                'response' => $singleActionResponse,
            ];
        }

        return $response;
    }

    /**
     * @param string $name
     * @param string $visibility (public||private)
     * @return array
     *
     * @todo Use Params class later
     */
    public function createProject(string $path, string $name = null, string $visibility = 'public', bool $compactResponse = true, bool $echo = true): array
    {
        $name = $name ?? $path;

        $response = Curl::json(
            "https://{$this->host}/api/v4/projects",
            (new CurlParams())
                ->method('POST')
                ->headers(["PRIVATE-TOKEN: " . $this->privateToken])
                ->urlQueryData(['path' => $path, 'name' => $name, 'visibility' => $visibility])
        );

        $id = @$response['id'];
        $url = @$response['http_url_to_repo'];

        if (empty($id) || empty($url)) {
            return [
                'status' => 'failure',
                'request' => ['path' => $path, 'name' => $name, 'visibility' => $visibility],
                'response' => $response,
            ];
        }

        if ($echo) {
            echo "Gitlab project created. ID: $id, URL: $url" . PHP_EOL;
        }


        return $compactResponse ? [$id, $url] : $response;
    }

    private function getProjectId(string $projectPath): int
    {
        if (empty($this->projects)) {
            $this->setProjectsData();
        }

        if (!array_key_exists($projectPath, $this->projects)) {
            throw new \Exception("Project path '$projectPath' cannot be found");
        }

        return $this->projects[$projectPath]['id'];
    }

    public function deleteProject(string $projectPath): array
    {
        $projectId = $this->getProjectId($projectPath);

        $response = Curl::json(
            "https://{$this->host}/api/v4/projects/$projectId",
            (new CurlParams())
                ->method('DELETE')
                ->headers(["PRIVATE-TOKEN: " . $this->privateToken])
        );

        return $response;
    }

    public function deleteLabel(string|int $projectPathOrId, string|int $labelTitleOrId): array
    {
        $projectId = is_string($projectPathOrId) ? $this->getProjectId($projectPathOrId) : $projectPathOrId;

        $response = Curl::json(
            "https://{$this->host}/api/v4/projects/$projectId/labels/$labelTitleOrId",
            (new CurlParams())
                ->method('DELETE')
                ->headers(["PRIVATE-TOKEN: " . $this->privateToken])
        );

        return $response === null ? ['success'] : $response;
    }

    // @todo Collect issues from all pages
    private function listIssues()
    {
        $response = Curl::json(
            "https://{$this->host}/api/v4/issues",
            (new CurlParams())
                ->method('GET')
                ->headers(["PRIVATE-TOKEN: " . $this->privateToken])
                ->urlQueryData(['state' => 'opened', 'per_page' => 100, 'page' => 1])
        );

        if(count($response) === 100){
            throw new \Exception("Issue amount limit is reached");
        }

        return $response;
    }

    public function listAllIssuesWithLabels()
    {
        $result = [];

        foreach ($this->listIssues() as $issue) {
            $result[] = [
                'id' => $issue['id'],
                'iid' => $issue['iid'],
                'project_id' => $issue['project_id'],
                'title' => $issue['title'],
                'labels' => $issue['labels'],
            ];
        }

        return $result;
    }

    public function addLabelsToIssue(string|int $projectPathOrId, int $issueIid, string $labelsString)
    {
        $projectId = is_string($projectPathOrId) ? $this->getProjectId($projectPathOrId) : $projectPathOrId;

        $response = Curl::json(
            "https://{$this->host}/api/v4/projects/$projectId/issues/$issueIid",
            (new CurlParams())
                ->method('PUT')
                ->headers(["PRIVATE-TOKEN: " . $this->privateToken])
                ->postRequestData(['add_labels' => $labelsString])
        );

        return $response;
    }
}