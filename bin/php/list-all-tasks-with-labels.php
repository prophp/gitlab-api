<?php

require_once __DIR__ . "/bootstrap.php";

use ProPhp\GitlabApi\ApiClient;

$ApiClient = new ApiClient($apiToken);

echo json_encode(
    $ApiClient->listAllIssuesWithLabels(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
);
