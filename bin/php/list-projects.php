<?php

require_once __DIR__ . "/bootstrap.php";

use ProPhp\GitlabApi\ApiClient;

$ApiClient = new ApiClient($apiToken);

$response = $ApiClient->listProjects();

$list = [];

foreach ($response as $projectPath => $projectData) {
    $list[$projectPath] = "{$projectData['id']}:  {$projectData['http_url_to_repo']}  ";
}

ksort($list);

echo json_encode(
    $list, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
);
