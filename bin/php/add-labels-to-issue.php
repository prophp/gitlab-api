<?php

require_once __DIR__ . "/bootstrap.php";

use ProPhp\GitlabApi\ApiClient;

$ApiClient = new ApiClient($apiToken);

if (empty($argv[2])) {
    throw new Exception("Project path missing (\$argv[2])");
}

$projectPath = $argv[2];

// curl

echo json_encode(
    $ApiClient->addLabelsToIssue($projectPath, 1, '_ps270'), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
);
