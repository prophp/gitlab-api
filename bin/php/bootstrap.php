<?php

$rootPath = dirname(__DIR__, 2) . "/";
require_once $rootPath . "vendor/autoload.php";

$privateConfig = json_decode(file_get_contents($rootPath . "private.json"), true);

$namespace = $argv[1];

if(!array_key_exists($namespace, $privateConfig['apiToken'])){
    throw new Exception("API token for the namespace '$namespace' does not exist in '/private.json' file");
}

$apiToken = $privateConfig['apiToken'][$namespace];
