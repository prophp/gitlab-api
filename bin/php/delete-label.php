<?php

require_once __DIR__ . "/bootstrap.php";

use ProPhp\GitlabApi\ApiClient;

$ApiClient = new ApiClient($apiToken);

if (empty($argv[2])) {
    throw new Exception("Project path missing (\$argv[2])");
}

if (empty($argv[3])) {
    throw new Exception("Label title/id missing (\$argv[3])");
}

echo json_encode(
    $ApiClient->deleteLabel($argv[2], $argv[3]), JSON_PRETTY_PRINT
);