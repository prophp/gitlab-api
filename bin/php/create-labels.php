<?php

require_once __DIR__ . "/bootstrap.php";

use ProPhp\GitlabApi\ApiClient;

// @todo Move to ApiClient::$defaultLabelsConfig
/** Old version */
$labelsConfig = [
    ['name' => 'Difficulty: low', 'color' => '#009966', 'priority' => 1],
    ['name' => 'Difficulty: medium', 'color' => '#ed9121', 'priority' => 2],
    ['name' => 'Difficulty: high', 'color' => '#ff0000'],

    ['name' => 'Importance: high', 'color' => '#dc143c', 'priority' => 11],
    ['name' => 'Importance: medium', 'color' => '#cd5b45', 'priority' => 12],
    ['name' => 'Importance: low', 'color' => '#8fbc8f'],
];

$ApiClient = new ApiClient($apiToken);

if (empty($argv[2])) {
    throw new Exception("Project path missing (\$argv[2])");
}

$projectPath = $argv[2];

echo json_encode(
    $ApiClient->createLabels($projectPath, $labelsConfig), JSON_PRETTY_PRINT
);

