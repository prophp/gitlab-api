<?php

require_once __DIR__ . "/bootstrap.php";

use ProPhp\GitlabApi\ApiClient;

$ApiClient = new ApiClient($apiToken);

if (empty($argv[2])) {
    throw new Exception("Project path missing (\$argv[2])");
}

$projectPath = $argv[2];
$projectName = $argv[3] ?? $projectPath;
$visibility = $argv[4] ?? 'public';

echo json_encode(
    $ApiClient->createProject($projectPath, $projectName, $visibility), JSON_PRETTY_PRINT
);