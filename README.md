# Docker

```
docker/run
```

# Executable files

## Universal executable 

(can execute any unique executable + all scripts in '/bin/php')

P.S. Create symbolic links with `cd bin; ln -s boilerplate create-project`

---
`bin/exec <php script basename>`
```
bin/exec create-labels prophp docker-bridge
```

## Unique executable files (auto suggestions enabled)

### Read

---
`bin/list-projects <namespace>`
```
bin/list-projects prophp
```
```
bin/exec list-projects prophp
```

### Create

---
`bin/create-labels <namespace> <project-path>`
```
bin/create-labels prophp docker-bridge
```
```
bin/exec create-labels prophp docker-bridge
```

---
`bin/create-project <namespace> <project-path> '<project-name>' <visibility>`
```
bin/create-project prophp test-project 'Test project' private
```
```
bin/exec create-project prophp test-project 'Test project' private
```

---
`bin/delete-project <namespace> <project-path>`
```
bin/delete-project prophp test-project
```
```
bin/exec delete-project prophp test-project
```