<?php

require_once "bootstrap.php";

use ProPhp\GitlabApi\ApiClient;

$ApiClient = new ApiClient($apiToken);

$labelsConfig = [
    ['name' => 'Importance: high', 'color' => '#dc143c', 'priority' => 1],
    ['name' => 'Importance: medium', 'color' => '#cd5b45', 'priority' => 2],
    ['name' => 'Importance: low', 'color' => '#8fbc8f'],
];

echo json_encode(
    $ApiClient->createLabels('docker-bridge', $labelsConfig), JSON_PRETTY_PRINT
);

// https://gitlab.com/prophp/docker-bridge/-/labels