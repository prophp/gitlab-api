<?php

require_once "bootstrap.php";

use ProPhp\GitlabApi\ApiClient;

$ApiClient = new ApiClient($apiToken);

echo json_encode(
    $ApiClient->listProjects(), JSON_PRETTY_PRINT
);