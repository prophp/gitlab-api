<?php

$rootPath = dirname(__DIR__) . "/";
require_once $rootPath . "vendor/autoload.php";

$privateConfig = json_decode(file_get_contents($rootPath . "private.json"));

$apiToken = $privateConfig->apiToken->prophp;