<?php

require_once "bootstrap.php";

use ProPhp\GitlabApi\ApiClient;

$ApiClient = new ApiClient($apiToken);

echo json_encode(
    $ApiClient->createProject('test-project', 'Test project'), JSON_PRETTY_PRINT
);

echo json_encode(
    $ApiClient->deleteProject('test-project'), JSON_PRETTY_PRINT
);